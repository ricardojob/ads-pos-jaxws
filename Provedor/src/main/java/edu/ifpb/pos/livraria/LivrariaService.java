package edu.ifpb.pos.livraria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jws.WebService;

/**
 *
 * @author Fermin Gallego
 *
 */
@WebService(endpointInterface
        = "edu.ifpb.pos.livraria.Livraria",
        serviceName = "LivrariaService")
public class LivrariaService implements Livraria {

    private final HashMap<String, Livro> livros = new HashMap<String, Livro>();

    public List<String> LocalizarLivros(String text) {
        List<String> titulosEncontrados = new ArrayList<String>();
        for (String title : livros.keySet()) {
            if (title.contains(text)) {
                titulosEncontrados.add(title);
            }
        }
        return titulosEncontrados;
    }

    public Livro recuperarLivro(String titulo) {
        return livros.get(titulo);
    }

    public void salvar(Livro livro) {
        livros.put(livro.getTitulo(), livro);
    }

}
