package edu.ifpb.pos.livraria;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Fermin Gallego
 *
 */
@WebService
public interface Livraria {

    @WebMethod
    public List<String> LocalizarLivros(String text);

    @WebMethod
    public Livro recuperarLivro(String title);

    @WebMethod
    public void salvar(Livro eBook);

}
