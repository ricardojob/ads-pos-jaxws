package edu.ifpb.pos.livraria;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fermin Gallego
 *
 */
public class Livro {

    private String titulo;
    private int numeroPagina;
    private double preco;

    public Livro() {
        super();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getNumeroPagina() {
        return numeroPagina;
    }

    public void setNumeroPagina(int numeroPagina) {
        this.numeroPagina = numeroPagina;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

}
