package edu.ifpb.pos.calc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Ricardo Job
 */
@WebService(serviceName = "Calculadora")
public class CalculadoraService implements Calculadora {

    public long soma(long a, long b) {
        return a + b;
    }
//    @WebMethod(operationName = "soma")
//    @Override
//    public long soma(@WebParam(name = "a") long a,
//            @WebParam(name = "b") long b) {
//        return a + b;
//    }

    public long subtrair(long a, long b) {
        return a - b;
    }
}
