package edu.ifpb.pos.livraria;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java de livro complex type.
 *
 * <p>
 * O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro
 * desta classe.
 *
 * <pre>
 * &lt;complexType name="livro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroPagina" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="preco" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="titulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "livro", propOrder = {
    "numeroPagina",
    "preco",
    "titulo"
})
public class Livro {

    protected int numeroPagina;
    protected double preco;
    protected String titulo;

    public Livro() {
    }

    public Livro(int numeroPagina, double preco, String titulo) {
        this.numeroPagina = numeroPagina;
        this.preco = preco;
        this.titulo = titulo;
    }

    /**
     * Obtém o valor da propriedade numeroPagina.
     *
     */
    public int getNumeroPagina() {
        return numeroPagina;
    }

    /**
     * Define o valor da propriedade numeroPagina.
     *
     */
    public void setNumeroPagina(int value) {
        this.numeroPagina = value;
    }

    /**
     * Obtém o valor da propriedade preco.
     *
     */
    public double getPreco() {
        return preco;
    }

    /**
     * Define o valor da propriedade preco.
     *
     */
    public void setPreco(double value) {
        this.preco = value;
    }

    /**
     * Obtém o valor da propriedade titulo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Define o valor da propriedade titulo.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTitulo(String value) {
        this.titulo = value;
    }

}
