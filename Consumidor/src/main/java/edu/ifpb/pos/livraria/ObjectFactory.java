
package edu.ifpb.pos.livraria;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the edu.ifpb.pos.livraria package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Salvar_QNAME = new QName("http://livraria.pos.ifpb.edu/", "salvar");
    private final static QName _RecuperarLivroResponse_QNAME = new QName("http://livraria.pos.ifpb.edu/", "recuperarLivroResponse");
    private final static QName _LocalizarLivrosResponse_QNAME = new QName("http://livraria.pos.ifpb.edu/", "LocalizarLivrosResponse");
    private final static QName _SalvarResponse_QNAME = new QName("http://livraria.pos.ifpb.edu/", "salvarResponse");
    private final static QName _LocalizarLivros_QNAME = new QName("http://livraria.pos.ifpb.edu/", "LocalizarLivros");
    private final static QName _RecuperarLivro_QNAME = new QName("http://livraria.pos.ifpb.edu/", "recuperarLivro");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: edu.ifpb.pos.livraria
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecuperarLivro }
     * 
     */
    public RecuperarLivro createRecuperarLivro() {
        return new RecuperarLivro();
    }

    /**
     * Create an instance of {@link SalvarResponse }
     * 
     */
    public SalvarResponse createSalvarResponse() {
        return new SalvarResponse();
    }

    /**
     * Create an instance of {@link LocalizarLivros }
     * 
     */
    public LocalizarLivros createLocalizarLivros() {
        return new LocalizarLivros();
    }

    /**
     * Create an instance of {@link LocalizarLivrosResponse }
     * 
     */
    public LocalizarLivrosResponse createLocalizarLivrosResponse() {
        return new LocalizarLivrosResponse();
    }

    /**
     * Create an instance of {@link RecuperarLivroResponse }
     * 
     */
    public RecuperarLivroResponse createRecuperarLivroResponse() {
        return new RecuperarLivroResponse();
    }

    /**
     * Create an instance of {@link Salvar }
     * 
     */
    public Salvar createSalvar() {
        return new Salvar();
    }

    /**
     * Create an instance of {@link Livro }
     * 
     */
    public Livro createLivro() {
        return new Livro();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Salvar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "salvar")
    public JAXBElement<Salvar> createSalvar(Salvar value) {
        return new JAXBElement<Salvar>(_Salvar_QNAME, Salvar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarLivroResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "recuperarLivroResponse")
    public JAXBElement<RecuperarLivroResponse> createRecuperarLivroResponse(RecuperarLivroResponse value) {
        return new JAXBElement<RecuperarLivroResponse>(_RecuperarLivroResponse_QNAME, RecuperarLivroResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalizarLivrosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "LocalizarLivrosResponse")
    public JAXBElement<LocalizarLivrosResponse> createLocalizarLivrosResponse(LocalizarLivrosResponse value) {
        return new JAXBElement<LocalizarLivrosResponse>(_LocalizarLivrosResponse_QNAME, LocalizarLivrosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalvarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "salvarResponse")
    public JAXBElement<SalvarResponse> createSalvarResponse(SalvarResponse value) {
        return new JAXBElement<SalvarResponse>(_SalvarResponse_QNAME, SalvarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalizarLivros }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "LocalizarLivros")
    public JAXBElement<LocalizarLivros> createLocalizarLivros(LocalizarLivros value) {
        return new JAXBElement<LocalizarLivros>(_LocalizarLivros_QNAME, LocalizarLivros.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarLivro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://livraria.pos.ifpb.edu/", name = "recuperarLivro")
    public JAXBElement<RecuperarLivro> createRecuperarLivro(RecuperarLivro value) {
        return new JAXBElement<RecuperarLivro>(_RecuperarLivro_QNAME, RecuperarLivro.class, null, value);
    }

}
