package edu.ifpb.pos.consumidor;

import edu.ifpb.pos.calculadora.Calculadora;
import edu.ifpb.pos.calculadora.CalculadoraService;
import edu.ifpb.pos.livraria.Livraria;
import edu.ifpb.pos.livraria.LivrariaService;
import edu.ifpb.pos.livraria.Livro;
import java.util.List;

/**
 *
 * @author Ricardo Job
 */
public class Principal {

    public static void main(String[] args) {
        /*Testando Calculadora*/
        Calculadora calculadora = new Calculadora();
        CalculadoraService calc = calculadora.getCalculadoraServicePort(); 
        System.out.println("Soma: " + calc.soma(13, 14));

        /*Testando a Livraria*/
        LivrariaService service = new LivrariaService();
        Livraria livraria = service.getLivrariaServicePort();

        Livro java = new Livro(105, 27.5, "Java como Programar");
        Livro c = new Livro(103, 26.5, "C++ como Programar");
        livraria.salvar(java);
        livraria.salvar(c);

        List<String> lista = livraria.localizarLivros("como");

        for (String string : lista) {
            System.out.println(string);
        }

    }
}
